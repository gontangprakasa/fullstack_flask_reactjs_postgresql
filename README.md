# Proses Instalasi
## _
Back End : Flask API
Front End : React JS
Database : PostgreSQL
- URL GIT --
## API ( Flask )
- Untuk mengaktifkan mode development pastikan anda sudah masuk ke project env (yang sudah disedikana)
```sh
 API git:(master) ✗ source env/bin/activate
```
- Import semuaa requirement yang sudah disedikan di requirement.txt
```sh
(env) API git:(master) ✗ pip install -r requirements.txt
```
- Pastikan nama database username, password dan host sudah disetting di env
- Migrate dan upgrade database kedalam management database anda (untuk case ini postgreeSQL)
```sh
(env) API git:(master) ✗ flask db init
(env) API git:(master) ✗ flask db migrate -m "buat table baru"
(env) API git:(master) ✗ flask db upgrade
```
- Jika dirasa semua sudah bisa dijalankan kemudian running API Flasknya

```sh
(env) API git:(master) ✗ flask run
```
- default port http://127.0.0.1:5000/
- Developer sudah menyediakan collection API untuk diolah di repo

## Front End ( ReactJS )
- sebelum merunning aplikasi pastikan anda sudah menginstall npm dan npm install -g create-react-app
- pastikan anda sudah memasukin folder reactJS
- pastikan anda baca requirements.txt yang sudah disediakan developer
- install semua packgae yang sudah disediakan di requirements.txt
```sh
(env) API git:(master) ✗ npm install --save reactstrap
(env) API git:(master) ✗ npm i react-router-dom --save
(env) API git:(master) ✗ npm install axios
(env) API git:(master) ✗ npm install react-dotenv

```
- jika dirasa sudah tidak ada error kemudian setting di .env
- pastikan url API sudah sesuai
- Jika dirasa semua sudah bisa dijalankan kemudian running ReactJSnya

```sh
(env) API git:(master) ✗ npm start
```
- default port http://127.0.0.1:3000/
