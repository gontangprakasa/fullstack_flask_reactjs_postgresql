import React , { useContext} from 'react'
import {
    Jumbotron,
    Button
  } from 'reactstrap';
import { AuthContext } from '../App';
import {Redirect} from 'react-router-dom'
import {Link} from 'react-router-dom'

function Dashboard () {
    const {state,dispatch} = useContext(AuthContext)
    console.log(state);
    if (!state.isAuthenticated) {
        return <Redirect to="/login"/>
    }

    return (
        <Jumbotron>
            <h1 className="display-3">Hi {state.user}</h1>
            <p className="lead">PT Widya Informasi Nusantara atau Widya Wicara merupakan bagian dari Widya Group Indonesia yang didirikan pada 26 Februari 2019.</p>
            <hr className="my-2" />
            <p>Kami menciptakan solusi untuk berbagai kebutuhan masyarakat Indonesia yang berkaitan dengan pemrosesan suara atau bahasa.Sejauh ini kami telah membuat empat inovasi produk berbasis teknologi suara, yaitu Speaker Pintar Widya Wicara Prima, TTS Widya Wicara, STT Widya Wicara, Natural Language Processing, dan Wake Word Arunika.</p>
            <p className="lead">

                <Link to={{ pathname: "https://widyawicara.com/" }} target="_blank" >
                    <Button color="primary">Learn More</Button>
                </Link>
            </p>
        </Jumbotron>
    )
}

export default Dashboard