import React , { useContext, useState, useEffect} from 'react'
import {
    Table,
    Container,
    Button
  } from 'reactstrap';
import { AuthContext } from '../../App';
import axios from 'axios';
import {Redirect,Link,NavLink} from 'react-router-dom'

const api = process.env.REACT_APP_URL_API+'/products';
function Produk () {
    const [product, setProduct] = useState([]);
    const {state} = useContext(AuthContext)
    
    const fetchData = () => {
        var config = {
            headers : {
                'Content-type' : 'application/json',
                'Authorization' : 'Bearer ' + state.token
            }
        }

        axios.get(api,config).then(res => {
            setProduct(res.data.data)
        }).catch(error => {
            console.log(error)
        })
    }

    useEffect( () => {
        fetchData()

    },[])

    const HapusProduct = (id) => {
        var config = {
            headers : {
                'Content-type' : 'application/json',
                'Authorization' : 'Bearer ' + state.token
            }
        }

        axios.delete(api+`/`+id,config).then(res => {
            fetchData()
        }).catch(error => {
            console.log(error)
        })
    }

    if (!state.isAuthenticated) {
        return <Redirect to="/login"/>
    }
    return (
        <div>
           <Container>
                    <h2>List Produk</h2>
                    <NavLink to="/produk/tambah"><Button className="success">Tambah Data</Button></NavLink>
                    <br/>
                    <Table className="table-bordered">
                        <thead>
                            <tr>
                                <th>Nama Produk</th>
                                <th>Harga Produk</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            {product.map( product =>
                                <tr key={ product.id }>
                                    <td>{ product.product_name }</td>
                                    <td>{ product.product_price }</td>
                                    <td> 
                                    <Link to= {
                                            {
                                                pathname : "/produk/edit",
                                                state : {
                                                    id : product.id,
                                                    product_name : product.product_name,
                                                    product_price : product.product_price
                                                }
                                            }
                                        }>
                                        <Button>Edit</Button>
                                        </Link>
                                        <span></span>
                                        <Button color="danger" onClick={() => HapusProduct(product.id)}>Delete</Button>
                                    </td>
                                </tr>
                            )}
                        </tbody>
                    </Table>
                </Container>
        </div>
    )
}

export default Produk