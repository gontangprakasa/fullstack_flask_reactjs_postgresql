import React from 'react'
import {
    Jumbotron,
    Button
  } from 'reactstrap';
import {Link} from 'react-router-dom'
function Home () {
     
    return (
        <div>
        <Jumbotron>
            <h1 className="display-3">Widya Wicara</h1>
            <p className="lead">PT Widya Informasi Nusantara atau Widya Wicara merupakan bagian dari Widya Group Indonesia yang didirikan pada 26 Februari 2019.</p>
            <hr className="my-2" />
            <p>Kami menciptakan solusi untuk berbagai kebutuhan masyarakat Indonesia yang berkaitan dengan pemrosesan suara atau bahasa.Sejauh ini kami telah membuat empat inovasi produk berbasis teknologi suara, yaitu Speaker Pintar Widya Wicara Prima, TTS Widya Wicara, STT Widya Wicara, Natural Language Processing, dan Wake Word Arunika.</p>
            <p className="lead">

                <Link to={{ pathname: "https://widyawicara.com/" }} target="_blank" >
                    <Button color="primary">Learn More</Button>
                </Link>
            </p>
        </Jumbotron>
    </div>
    )
}

export default Home