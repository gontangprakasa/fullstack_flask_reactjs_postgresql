from app import app
from app.Controller import UserController
from app.Controller import ProductController
from flask import request, redirect
from flask_jwt_extended import get_jwt_identity
from flask_jwt_extended import jwt_required
import os
import sys  

@app.route('/')
def home():    
    return "Flask "+sys.version
@app.route('/login', methods=['POST'])
def login():
    return UserController.login()

@app.route('/save-super-admin', methods=['POST'])
def superamin():
    return UserController.saveSuperAdmin()

@app.route('/logout', methods=['POST'])
def logout():
    return UserController.logout()

@app.route('/products', methods=['GET','POST'])
@jwt_required()
def products():
    if request.method == 'GET' :
        return ProductController.index()
    else :
        return ProductController.save()
    
@app.route('/products/<id>', methods=['GET','PUT','DELETE'])
@jwt_required()
def productsId(id):
    if request.method == 'GET' :
        return ProductController.detail(id)
    elif request.method == 'PUT' :
        return ProductController.update(id)
    else :
        return ProductController.delete(id)
        
        

